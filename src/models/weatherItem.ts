function getWingDesc(deg:number) {
  if (deg < 22.5) {
    return 'S'
  } else if (deg < 67.5) {
    return 'SSW'
  } else if (deg < 112.5) {
    return 'W'
  } else if (deg < 157.5) {
    return 'NNW'
  } else if (deg < 202.5) {
    return 'N'
  } else if (deg < 247.5) {
    return 'NNE'
  } else if (deg < 292.5) {
    return 'E'
  } else if (deg < 337.5) {
    return 'SSE'
  } else {
    return '...'
  }
}

export class WeatherItem {
  place!: string
  time!: number
  phase!: string
  temp!: string
  temp_feel!: string
  humidity!: string
  pressure!: string
  wind_sp!: string
  wind_dir!: number
  wind_desc!: string
  visibility!: string
  weather_id!: string
  weather_main!: string
  weather_desc!: string

  constructor(place:string, res:any) {
    this.place = place
    this.time = res.dt !== undefined ? res.dt : -1
    this.phase = res.weather?.[0]?.icon !== undefined ? res.weather[0].icon.slice(-1) === 'd' ? 'day' : 'night' : 'day'
    this.temp = res.main?.temp !== undefined ? String(Math.round(res.main.temp)) : '...'
    this.temp_feel = res.main?.feels_like !== undefined ? String(Math.round(res.main.feels_like)) : '...'
    this.humidity = res.main?.humidity !== undefined ? String(Math.round(res.main.humidity)) : '...'
    this.pressure = res.main?.pressure !== undefined ? String(Math.round(res.main.pressure)) : '...'
    this.wind_sp = res.wind?.speed !== undefined ? res.wind.speed.toFixed(1) : '...'
    this.wind_dir = res.wind?.deg !== undefined ? res.wind.deg : 0
    this.wind_desc = getWingDesc(this.wind_dir)
    this.visibility = res.visibility !== undefined ? String(Math.round(res.visibility / 1000)) : '...'
    this.weather_id = res.weather?.[0]?.id !== undefined ? String(res.weather[0].id) : '...'
    this.weather_main = res.weather?.[0]?.main !== undefined ? String(res.weather[0].main) : '...'
    this.weather_desc = res.weather?.[0]?.description !== undefined ? String(res.weather[0].description) : '...'
  }
}