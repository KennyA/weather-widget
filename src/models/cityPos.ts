export class CityPos {
  place!: string
  lat!: number
  lon!: number
  
  constructor(place:string, lat:number, lon:number) {
    this.place = place
    this.lat = lat
    this.lon = lon
  }
}