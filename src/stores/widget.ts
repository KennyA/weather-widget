import { defineStore } from 'pinia'
import axios from 'axios'

import { CityPos } from '@/models/cityPos'
import { WeatherItem } from "@/models/weatherItem"

async function getWeather(api:string, cityPos:CityPos):Promise<WeatherItem | undefined> {
  const weather = await axios.get(
    `https://api.openweathermap.org/data/2.5/weather?lat=${cityPos.lat}&lon=${cityPos.lon}&units=metric&appid=${api}`
  )
  if (weather?.data !== undefined) {
    return new WeatherItem(cityPos.place, weather.data)
  }
  return undefined
}

export const useWidgetStore = defineStore('widget', {
  state: () => ({
    api: import.meta.env.VITE_OPEN_WEATHER_KEY as string,
    items: [] as WeatherItem[],
    itemCurrent: 0 as number,
    info: false as boolean,
    settings: false as boolean,
  }),
  getters: {
    getStoredCities():string[] {
      const localItems = localStorage.getItem('cities')
      if (localItems !== null) {
        return JSON.parse(localItems) as string[]
      }
      return []
    },
  },
  actions: {
    infoToggle(value:boolean):void {
      this.info = value !== undefined ? value : false
    },
    settingsToggle(value:boolean):void {
      this.settings = value !== undefined ? value : false
    },
    async addItemLocal():Promise<void> {
      this.settingsToggle(true)
      navigator.geolocation.getCurrentPosition(async (coord) => {
        if (coord?.coords?.latitude !== undefined && coord?.coords?.longitude !== undefined) {
          const lat = coord.coords.latitude
          const lon = coord.coords.longitude
          const place = await axios.get(
            `http://api.openweathermap.org/geo/1.0/reverse?lat=${lat}&lon=${lon}&limit=1&appid=${this.api}`
          )
          if (
            place.data?.[0]?.name !== undefined && 
            place.data[0].country !== undefined && 
            place.data[0].lat !== undefined && 
            place.data[0].lon !== undefined
          ) {
            const cityPos = new CityPos(
              `${place.data[0].name}, ${place.data[0].country}`,
              place.data[0].lat,
              place.data[0].lon
            )
            const item = await getWeather(this.api, cityPos)
            if (item !== undefined) {
              this.items = [item]
              this.updateCitiesStorage()
            }
            this.settingsToggle(false)
          }
        }
      })
    },
    async getItemByCityName(value:string):Promise<WeatherItem | undefined> {
      const place = await axios.get(
        `http://api.openweathermap.org/geo/1.0/direct?q=${value}&limit=1&appid=${this.api}`
      )
      if (place.data?.length > 0) {
        const cityPos = new CityPos(
          `${place.data[0].name}, ${place.data[0].country}`, 
          place.data[0].lat, 
          place.data[0].lon
        )
        return await getWeather(this.api, cityPos)
      }
      return undefined
    },
    addItem(item:WeatherItem):void {
      this.items.push(item)
      this.updateCitiesStorage()
    },
    deleteItem(index:number):void {
      this.items.splice(index, 1)
      this.updateCitiesStorage()
      this.fixCurrentItem()
    },
    resetItems():void {
      this.items = [] as WeatherItem[]
    },
    setEmptyItems(length:number):void {
      Array.from({ length }, () => {
        this.items.push(new WeatherItem('...', {}))
      })
    },
    async updateItems():Promise<void> {
      const cities = this.getStoredCities
      cities.forEach(async (city:string, index:number) => {
        const newItem = await this.getItemByCityName(city)
        if (newItem !== undefined) {
          this.items[index] = newItem
        }
      })
    },
    updateCitiesStorage() {
      const cities = this.items.map((item: WeatherItem) => {
        return item.place.toLowerCase().replace(', ', ',')
      })
      localStorage.setItem('cities', JSON.stringify(cities))
    },
    fixCurrentItem():void {
      if (this.itemCurrent > this.items.length - 1 && this.itemCurrent > 0) {
        this.itemCurrent = this.items.length - 1
      }
    },
  },
})