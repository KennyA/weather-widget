import { defineCustomElement } from "vue"
import { createPinia, setActivePinia } from "pinia"
import WeatherWidget from './components/WeatherWidget.ce.vue'

setActivePinia(createPinia());

const WeatherWidgetCustom = defineCustomElement(WeatherWidget)
customElements.define('weather-widget', WeatherWidgetCustom)